# paramikoAlpha

paramikoAlpha is mainly an automation platform project called ptat.  
There are additional "hello world" experiments in this repo that are not a part of the automation project.

ptat stands for python test automation tool.

The goals of this "proof of concept" are to:
   - control multiple ssh connections in parallel
       - the user would write a layer on top of these connections per device type
       - other interfaces can be supported such as Rest.
   - run a test list from a file
       - the test list consists of test title, test tags, python test function and arguments
   - control the test list flow with a gui 
       - pause or abort testing at anytime
   - be able to edit code on the fly and retry failing functions / tests
       - edit and reload edited code without restarting ssh connections

Tool components:
   - ptat runs with ipython so the user has a prompt to reload python code.

   - the vsh.py library to automate ssh (built on the python library paramiko)
      - allows the user to connect and send and recieve and disconnect from ssh connections.  There is a simple tk gui for each connection.

   - runTestListData.py for running the testList.txt test file
      - note
        - a goal is to have the data after the functions in columns so that test combinations can be read easily
        - there will be multiple column groups of function + data:  title tags function1 dataA dataB function2 dataC dataD ...

   - 

# installation requirements for windows
  - installed python for windows 3.7
    ... location ...
  - installed git for windows
    https://github.com/git-for-windows/git/releases/download/v2.34.1.windows.1/Git-2.34.1-64-bit.exe
	then git run from a bash shell
  - installed pip
    copied get-pip.py to python directory
  - updated system PATH to include
    python directory
  - from python37 directory
     - pip install ipython
     - pip install paramiko
  - from your code directory
    git clone https ( to this repo )
    
  - edit demo1Setup.py login information for a valid node or sim.

# run the demo
&#35; start two ipython shells<br>

&#35; In ipython shell 1<br>
  cd .../paramikoAlpha<br>
  ./testSuiteControlUI.py

&#35; In ipython shell 2<br>
  cd .../paramikoAlpha<br>
  ipython<br>
  &#35; starts one or two ssh connections<br>
  run -i demo1Setup.py

  &#35; show commands like<br>
  rslt = vsh1.sendRcv('help\n')<br>
  &#35; and one with reg exp

  &#35; Then discuss the test functions and test list

  &#35; Then start test list / test suite<br>
  run -i runTestListData.py


