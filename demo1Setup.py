# System Automation Demo1

"""
to do:
   - put TK into vsh (paramRG)
"""

# ipython

# more selective import ???
import time
import threading
import random
import queue
import code
import logging
import os
import importlib
import re as re
import vsh
from testFunctionLib import *

# start the ipython log and link ipython event loop to TK UI loop
get_ipython().run_line_magic('logstart','')
get_ipython().run_line_magic('gui','tk')

try:
   logging.shutdown()
except:
   noop = 1

logging.basicConfig(filename='vsh.log',level=logging.DEBUG)
logging.debug('This message should go to the log file')
logging.info('So should this')
logging.warning('And this, too')
logger = logging.getLogger(__name__)

vsh1 = vsh.Vsh(1,logger)
#vsh2 = vsh.Vsh(2,logger)

# or run with local host as in prev version(s)
vsh1.connect("1.1.3.xx","22","yyy","zzz")
#vsh2.connect()
print("after connect")
rslt = vsh1.sendRcv('help\n')
print("pre rslt")
print(rslt)
print("post rslt")
#rslt2 = vsh2.sendRcv('ls -all\n')

print(
"""
help:
   - reload (tune this)        
      vsh*.disconnect()
      importlib.reload(vsh)
   - logging:           'logging.info(\'So should this\')'
   - halt running code: type ctl-c
   - exit:              'exit'

demo1 ( paste these commands )
ipython

# cd /Users/rgenet/gitProjs/gitlab/paramikoAlpha/

# loads libs needed
# starts logger for vsh
# print out help
# enables vsh module/class to load

run -i demo1Setup.py
or
exec(open("filename.py").read())

vsh1 = vsh.Vsh(1,logger)
vsh2 = vsh.Vsh(2,logger)

vsh1.connect()
vsh2.connect()

rslt = vsh1.sendRcv('ls\n')
rslt2 = vsh2.sendRcv('ls -all\n')

rslt3 = vsh1.sendRcv('ps -ef | grep a\n')
rslt4 = vsh2.sendRcv('top -l 1\n')

# CPU usage: 24.12% user, 39.69% sys, 36.18% idle 
matchObj = re.search( r'^.*CPU usage: ([0-9]+\.[0-9]+)\% user\, ([0-9]+\.[0-9]+)\% sys\, ([0-9]+\.[0-9]+)\% idle .*$', rslt4, flags=re.MULTILINE)
if matchObj:
   #print("matchObj.group() : ", matchObj.group())
   #print("matchObj.groups() : ", matchObj.group())
   print("matchObj.group(1) : ", matchObj.group(1))
   print("matchObj.group(2) : ", matchObj.group(2))
   print("matchObj.group(3) : ", matchObj.group(3))

vsh1.sendRcv('ls -all\n',sendOnly=True)
vsh2.sendRcv('ls | grep a\n',sendOnly=True)

r1 = vsh1.sendRcv('',rcvOnly=True)
r2 = vsh2.sendRcv('',rcvOnly=True)

#vsh1.disconnect(1)
#vsh2.disconnect(2)

run -i <pathTo>/paramikoAlpha/runTestListData.py
"""
)

