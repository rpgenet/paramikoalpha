import sys
from time import sleep
from netmiko import ConnectHandler

dev={
   'device_type': 'linux',
   'host': 'localhost',
   'username': 'rgenet',
   'password': 'blabla',
} # Use a dictionary to pass the login parameters

# Connect to the device
ssh_conn = ConnectHandler(**dev)
ssh2 = ConnectHandler(**dev)

# Send the command and print the result
print(ssh_conn.send_command("ls -l\n"))
print(ssh2.send_command("ps -ef | grep hi\n"))