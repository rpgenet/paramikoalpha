

"""
 - decorators - home made
 - import issue



"""

import functools

# decorator
def func1():
   print("in func1")

def my_decorator(func):
   @functools.wraps(func)
   def wrapper(*args,**kwargs):
      print("before")
      myret = func(*args,**kwargs)
      print("aafter")
      return myret
   return wrapper

funcx = my_decorator(func1)
funcx()

# https://realpython.com/primer-on-python-decorators/
@my_decorator
def func2(mya):
   print(f"in func2 {mya}")
   return("haha")

func2("inputStr")



# impor issue
import datetime
datetime.datetime.now()
f = datetime.datetime.now()
print(f)
datetime.datetime.now().hour
# this import will remove datetime namespace for "now()"
from datetime import datetime
datetime.now().hour
# error
datetime.datetime.now().hour
