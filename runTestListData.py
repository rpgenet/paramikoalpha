# 
# cd /Users/rgenet/gitProjs/gitlab/paramikoAlpha
#
# ipython
# run -i demo1Setup.py
# run -i runTestListData.py
#
# python3 
# demo1Setup.py
# run -i runTestListData.py
# 
#

import os
import time
import testFunctionLib

my_file = open("/Users/rgenet/gitProjs/gitlab/paramikoAlpha/testList.txt", "r")
content_list = my_file.readlines()

# hard code run all for now:
abort_on_fail = False
skip_failed_tests = False
##### options
if ('abort_on_fail' not in locals()) or ('abort_on_fail' not in globals()):
   abort_on_fail = True
if ('skip_failed_tests' not in locals()) or ('skip_failed_tests' not in globals()):
   skip_failed_tests = True
print("Run test List Options:")
print("abort_on_fail == ",abort_on_fail)
print("skip_failed_tests == ",skip_failed_tests)
###### options end
# https://stackoverflow.com/questions/3061/calling-a-function-of-a-module-by-using-its-name-a-string
# this is a tab'd file vs a spread-sheet.  Consider spread-sheet.

fullL = []
for i in content_list:
    row = i.rstrip()
    if row.startswith("#") == False:
        rowSplitL = row.split("\t")
        nonNullItems = []
        for i in rowSplitL:
            if i != "":
                nonNullItems.append(i)
        rowBuilt = []
        rowBuilt.append(nonNullItems[0])
        rowBuilt.append(nonNullItems[1])
        #print("this is a list range",nonNullItems[2])
        rowBuilt.append(nonNullItems[2])
        #range(nonNullItems[2,-1])
        #rowBuilt.append(' '.join()
        #print("row ",rowBuilt,"len row ", len(rowBuilt))
        #print("row ",nonNullItems,"len row ", len(nonNullItems))
        fullL.append(rowBuilt)
        # debug print:
        #print("split row len",len(rowBuilt),"row",rowBuilt)
#print(fullL)

#print("Full List Len ",len(fullL))


failed_test = False
if skip_failed_tests == False:
   failed_test_num = 0
# failed_test_num = 0
if ('failed_test_num' not in locals()) or ('failed_test_num' not in globals()):
   # failed test becomes the place to start in the list 
   failed_test_num = 0
else:
   noop = 1
   # start AFTER the last fail ( current behavior)
   # if failed_test_num == 0:
   #    # did not fail
   #    noop = 1
   # else:
   #    # failed, need to start at next row.
   #    failed_test_num += 1

test_row = 1
for i in fullL:
   failed_test = False
   rslte = None
   if os.path.exists('/Users/rgenet/autoStop.txt'):
      print('Abort has been set')
      break
   if os.path.exists('/Users/rgenet/autoPause.txt'):
      print('Paused testing...')
      while os.path.exists('/Users/rgenet/autoPause.txt'):
         print('.', end = '')
         time.sleep(1)
         #vsh.rootn.update()
      print('Resuming testing...')
   print("---------------- Test Row = ",test_row, " begin ------------------")
   if (test_row > failed_test_num) or (skip_failed_tests == False):
      #print(i,len(i))
      print("   executing test title -",i[0])
      print("   executing command -",i[2],"-")
      # or exec.
      #print("not evaling")
      try:
         #print("   eval(i[2]) i[2]- ",i[2]," -")
         rslte = eval(i[2])
         if (rslte == "0") or (rslte == None):
            print("   Failed Test: return failed for ",i[2]," = -",rslte,"-")
            failed_test = True
         else:
            noop = 1
      # not sure this is working !!!
      #  as msg:  
      except Exception as msg:  
         print("   Failed Test: eval failed for ",i[2])
         print("      --- error begin ---")
         print(msg)
         print("      --- error end ---")
         failed_test = True
      except AssertionError as msg:  
         print("   Failed Test: eval failed for ",i[2])
         print("      --- error begin ---")
         print(msg)
         print("      --- error end ---")
         failed_test = True
      else:
         noop1 = 1
      finally:
         noop2 = 1 
      if failed_test == False:
         print("   Passed Test: ",i[2])
         print("      --- result begin (240 char max) ---")
         print(str(rslte)[0:240])
         print("      --- result end ---")
      else:
         print("   Failed Test: ",i[2])
         print("      --- result begin (240 char max) ---")
         print(str(rslte)[0:240])
         print("      ---result end ---")        
      # use abort_on_fail???
      print("---------------- Test Row = ",test_row, " end ------------------")
      if (failed_test == True) and (abort_on_fail == True):
         print("Aborting Test List: (failed_test == True) and (abort_on_fail == True)")
         failed_test_num = test_row
         exc = "Fail is set, will resume on test after failed test, clear with 'skip_failed_tests = False or failed_test_num = 0'"
         raise Exception(exc)
      test_row += 1
   else:
      print("Resuming testing after an abort - Skipping test: ",test_row)
      if test_row == failed_test_num:
         # clear the fail
         failed_test_num = 0
      test_row += 1
   print('   ...slowing test list down for demo, sleeping 3 sec...')
   time.sleep(3)


# l = []
# l.append("gg")
# l.append("h")
# l.append("j j")
# print(l)
# print(len(l))

# print("--")
# x = 'feat1_autotest1("a", "b")'
# eval(x)

