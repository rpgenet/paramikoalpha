#!/usr/local/bin/python3
import time
def mywait(secs):
   # currently unused!
   max = secs
   start = time.time()
   while True:
      ### Do other stuff, it won't be blocked
      time.sleep(0.1)
      print("looping...")

      ### This will be updated every loop
      remaining = max + start - time.time()
      print("%s seconds remaining" % int(remaining))

      ### Countdown finished, ending loop
      if remaining <= 0:
         break
   return
mywait(3)
