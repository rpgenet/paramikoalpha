#!/usr/local/bin/python3
"""
Two modes:
1) as main: ./testSuiteControlUI.py

2) from ipython:
cd /Users/rgenet/gitProjs/gitlab/paramikoAlpha
ipython
run -i testSuiteControlUI.py

# maybe
%gui tk

# no
#%gui osx


# ctl-c to kill main loop
# bt.root.destroy()
or
# x in window

importlib.reload(haltButton)
"""

#top2 = Tkinter.Tk()
import os
import tkinter 
import time
import importlib
top2 = tkinter.Tk(  )
top2.title('Test Suite Control')
top2.geometry('200x200') # Size 200, 200


class bt:
   def __init__(self,root,tkinter):
      # Set up the GUI
      self.root = root
      self.tkinter = tkinter

      # were globals
      #self.running1 = False
      self.running1 = True
   #def connect(self,connId):
   #def mbreak(self):
   #   eval(break)
      #self.poll(root,tkinter)

   # use this later:
   def poll(self,root,tkinter):
      self.root.update_idletasks()
      time.sleep(500)
      print("hi")
      self.root.after(250,self.poll(root,tkinter))

   def start(self):
      sec = 0
      self.running1 = True
      if os.path.exists('/Users/rgenet/autoStop.txt'):
         os.remove('/Users/rgenet/autoStop.txt')
      if os.path.exists('/Users/rgenet/autoPause.txt'):
         os.remove('/Users/rgenet/autoPause.txt')
      #importlib.reload(runTestListData)
      #import runTestListData
      while self.running1:
         #if sec % 1 == 0:
         print(".",sec)
         # ms!
         #time.sleep(.1)
         #title = f'halt on fail %',sec
         #self.root.title(title)
         sec += 1
         # 10
         if sec > 10:
            break
         if os.path.exists('/Users/rgenet/autoPause.txt'):
            print('Paused testing...')
            while os.path.exists('/Users/rgenet/autoPause.txt'):
               print('.', end = '')
               time.sleep(1)
               self.root.update()
            print('Resuming testing...')
         if os.path.exists('/Users/rgenet/autoStop.txt'):
            break
         #try:
         # not happy
         self.root.update()
         #print('ok')
         #self.root.update_idletasks()
         #self.root.mainloop()
         #self.root.after(2,self.root.update_idletasks())
         # test
         #self.root.after(2,self.mbreak())
         time.sleep(1)
         # except:
         #    noop = 1
         #    print("err:")

   def pause(self):
      fp = open('/Users/rgenet/autoPause.txt', 'w')
      fp.close()
      print('pause')
      self.root.update()

   def unpause(self):
      if os.path.exists('/Users/rgenet/autoPause.txt'):
         os.remove('/Users/rgenet/autoPause.txt')
      print('unpause')
      self.root.update()

   def stop(self):
      #global top2
      #top2.update()
      #self.root.update_idletasks()
      # 
      fp = open('/Users/rgenet/autoStop.txt', 'w')
      fp.close()
      self.running1 = False
      #self.root.mainloop()
      print('stop')
      #self.root.update_idletasks()
      self.root.update()
      #top2.update()

bt = bt(top2,tkinter)
startButton = tkinter.Button(top2, height=2, width=20, text ="Start", command = bt.start)
pauseButton = tkinter.Button(top2, height=2, width=20, text ="Pause", command = bt.pause)
unpauseButton = tkinter.Button(top2, height=2, width=20, text ="unPause", command = bt.unpause)
stopButton = tkinter.Button(top2, height=2, width=20, text ="Stop", command = bt.stop)

startButton.pack()
pauseButton.pack()
unpauseButton.pack()
stopButton.pack()
print('n3')
if __name__ == "__main__":
   bt.root.mainloop()