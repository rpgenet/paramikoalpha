import paramiko
import time
import tkinter
import gc
#from getpass import getpass
import logging

# 
# does not exist:
#    paramRG.parallelConnAndSend(1,1,"na",logger,root,T,tkinter)
#    ( replace with sendOnly x 2 and recvOnly x2(n))
#    - i.e. - separate send and rcv
#    make logger part of class too !!!
#
class Vsh:
   def __init__(self,connId,logger):
      self.logger = logger
      # Set up the GUI
      #self.root = root
      #self.T = T
      #self.tkinter = tkinter

      # were globals
      self.ssh = {}
      self.chan = {}
      self.chann = {}
      self.bufStr = {}
      self.connId = connId

   def connect(self,ip,port,username,password,**kwargs):
      # 
      connId = self.connId
      print("connect e")
      # connection ID not needed with "self" !!!
      # c1 = conection ID 1,2, ...
      # conn = 0,1,2 
      #   1 = new connection
      #   0 = send / rcv command
      #   2 = disconnect
      # cmd = command to send with \n
      # logger id
      # not working!

      if False:
         noop = 1
         # self.root = tkinter.Tk( )
         # cstr = "vsh connection #" + str(connId)
         # self.root.title(cstr)
         # self.T = tkinter.Text(self.root, height = 25, width = 80,)
         # self.T.pack()
         # self.T.insert(tkinter.END,"this is texdft\n")
      else:
         rootn = 'root' + str(connId)
         self.rootn = tkinter.Tk( )
         cstr = "vsh connection #" + str(connId)
         self.rootn.title(cstr)
         Tn = 'T' + str(connId)
         self.Tn = tkinter.Text(self.rootn, height = 25, width = 80,)
         self.Tn.pack()
         self.Tn.insert(tkinter.END,"this is texdft\n")
         # do toplevel and not another TK..
         # https://stackoverflow.com/questions/51054522/new-window-title-in-tkinter
         #self.T = tkinter.Text(self.root[connId], height = 25, width = 80,)

      # later check if connected already and return error and disconnect first suggestion

      self.ssh[connId] = paramiko.SSHClient()
      # 
      self.ssh[connId].set_missing_host_key_policy(paramiko.AutoAddPolicy())
      self.ssh[connId].load_system_host_keys()

      # https://stackoverflow.com/questions/7439563/how-to-ssh-to-localhost-without-password
      if 0:
         # missing key param above
         passwd = getpass()
         #ssh.connect(hostname="localhost",port = "22",username="rgenet",password="xxyyzz")
         self.ssh.connect(hostname="localhost",port = "22",username="rgenet",password=passwd)
      else:
         #self.ssh[connId].connect(hostname="localhost",port = "22")
         self.ssh[connId].connect(hostname = ip, port = port, username = username,password = password)
         print("...23 22")
      self.ssh[connId]._transport.set_keepalive(60)

      #
      #print("Timeout set short = .1 - because of upper level looping")
      #self.ssh[connId].settimeout(.1)
      # !!!
      # https://www.semicolonworld.com/question/60047/how-to-interact-with-paramiko-39-s-interactive-shell-session
      self.chan[connId] = self.ssh[connId].get_transport().open_session()
      self.chan[connId].get_pty()
      self.chan[connId].invoke_shell()
      return 1

   def sendRcv(self,cmd,**kwargs):
      # do these need to be self.* for returns?!
      #
      # optional args
      #
      # sendOnly=False(default)  or True
      # rcvOnly=False(default)  or True
      # timeout=20(default) - seconds

      connId = self.connId
      print("sendRcv l")
      timeouti = int(kwargs.get('timeout',20))
      sendOnlyi = kwargs.get('sendOnly',False)
      rcvOnlyi = kwargs.get('rcvOnly',False)
      execCmd = kwargs.get('execCmd',False)
   
      if self.ssh == {}:
         print("Please connect first")
         return "0"
      if execCmd == True:
         #elif:
         #if 'ssh[connId]' in globals():
         #print("*** chan[connId]",chan[connId],"***")
         stdin, stdout, stderr = self.ssh[connId].exec_command(cmd)
         for line in stdout:
            #print(line)
            self.Tn.insert(tkinter.END,line)
         return stdout
      # else leaving connection open

      # send first
      if rcvOnlyi == False:
         #self.chan[connId].send("\n")
         #time.sleep(0.1)
         # clear the buffer if 
         buffer = b''
         resp = b''
         reads = 0
         #chan+connId.send(cmd)
         #time.sleep(2)
         # https://stackoverflow.com/questions/14643861/paramiko-channel-stucks-when-reading-large-ouput
         # print("-CB1-")
         # maybe blocks if buffer empty

         # 
         if self.chan[connId].recv_stderr_ready():
            data = self.chan[connId].recv_stderr(1024)
            print("err",data)
            datal = len(data)
            dcountMax = 400
            # could save "event" output to a second buffer like with TL1 !!!
            if datal > 1023:
               print("Clear Buffer: this may break later, note dcount etc, arbitrary")
               dcount = 0
               while data and ( dcount < dcountMax ):
                  if self.chan[connId].recv_stderr_ready():
                     data = self.chan[connId].recv_stderr(1024)
                     print("err",data)
                     # temp test!!!
                     #self.root.update()
                     datal = len(data)
                     #print("-CB",end="")
                     if datal > 1023:
                        dcount += 1
                     else:
                        break
                  else:
                     break
               # print error if hit max!!!
               print('dcount = ',dcount)
               if dcount == dcountMax:
                  print ('dcount = dcountMax, did not fully clear buffer, not sending!')
                  return "0"
            del data
         # 
         if self.chan[connId].recv_ready():
            data = self.chan[connId].recv(1024)
            datal = len(data)
            dcountMax = 400
            # could save "event" output to a second buffer like with TL1 !!!
            if datal > 1023:
               print("Clear Buffer: this may break later, note dcount etc, arbitrary")
               dcount = 0
               while data and ( dcount < dcountMax ):
                  if self.chan[connId].recv_ready():
                     data = self.chan[connId].recv(1024)
                     # temp test!!!
                     #self.root.update()
                     datal = len(data)
                     #print("-CB",end="")
                     if datal > 1023:
                        dcount += 1
                     else:
                        break
                  else:
                     break
               # print error if hit max!!!
               print('dcount = ',dcount)
               if dcount == dcountMax:
                  print ('dcount = dcountMax, did not fully clear buffer, not sending!')
                  return "0"
            del data

      # send first
      if rcvOnlyi == False:
         print("sending ",cmd)
         self.chan[connId].send(cmd)

      if sendOnlyi == True:
         return "1"

      print("rcv == True")
      success = 0
      buffer = b''
      resp = b''
      reads = 0
 
      # 20 secs
      maxReads = int(timeouti*100)
      while reads < maxReads:
         # careful on sleep time ganularity
         try:
            #exec("root.update()",globals()) 
            rootn = 'root' + str(connId)
            self.rootn.update()
            # self.root.update()
            #print("&",end="")
         # e not yet working !!!
         except Exception as e:
            noop = 1
            print("error rootn update",end="")
         # self.__
         reads += 1
         if reads%10 == 0:
            time.sleep(0.1)
            print('reads w/sleep 0.1 = ',reads)
         if reads == maxReads:
            hitMaxReads = "yes"
         
         if self.chan[connId].recv_ready():
            resp = self.chan[connId].recv(1024)
            #print(resp)
            buffer += resp
            if resp.endswith(b'$ ') or resp.endswith(b'# ') or resp.endswith(b'> '):
               success = 1
               break
      del resp

      # local variable and e ( not working !!!)
      if 'e' in locals():
         print('exception:',str(e))
      print("reads ",reads)
      if success == 0:
         print('success = 0! -> failed')
         #self.bufStr[connId] = "0"
         bufStr = "0"
      else:
         # out.decode("ascii")
         # buffer.decode("utf-8")
         #self.bufStr[connId] = buffer.decode("ascii")
         noop = 1
      bufStr = buffer.decode("ascii")
      del buffer

      #self.logger.info(self.bufStr[connId])
      print("blen",len(bufStr))
      #self.bufStr[connId] = "\n".join(self.bufStr[connId].splitlines())
      bufStr = "\n".join(bufStr.splitlines())
      #lbufStr = self.bufStr[connId]
      print("alen",len(bufStr))
      Tn = 'T' + str(connId)
      # for line in bufStr.splitlines():
      #    self.Tn.insert(tkinter.END,bufStr)
      # del line
      self.Tn.insert(tkinter.END,bufStr)
      #print("clearing buffer string")
      #del self.bufStr[connId]
      #self.bufStr[connId] = ""
      return bufStr

   def disconnect(self):
      
      connId = self.connId
      #self.root[connId].destroy()
      rootn = 'root' + str(connId)
      self.rootn.destroy()
      self.chan[connId].close()
      self.ssh[connId].close()
      # need? Transport.close() or SSHClient.close()
      # https://github.com/paramiko/paramiko/issues/949

      # reclaim the conn ID info.
      # del self
      return "1"